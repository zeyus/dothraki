from otree.api import *
import random
import pandas as pd
from itertools import chain


doc = """
Training phase of the dothraki experiment, going till the participants have written 16 or 32 sentences correctly
"""


class C(BaseConstants):
    NAME_IN_URL = "learn_dothraki"
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 36 * 4  # max number of rounds, has to be preallocated :/

    NUM_SENTENCES = 72
    GOAL = {"high": 36, "low": 18}

    SENTENCE_DB = pd.read_csv("../dothraki/_static/dothraki_stim/sentences.csv")

    TREATMENT_CONFIG = [
        # {'L2': 1},
        {"L2": 3},
    ]

    AVATAR_PLAYER = "Arya"
    AVATAR_PARTNER = "Renly"
    AVATAR_OTHER = "Margaery"


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    sentence = models.IntegerField()
    answer = models.StringField()
    correct = models.BooleanField()


# FUNCTIONS
def creating_session(subsession: Subsession):
    if subsession.round_number == 1:
        ## TREATMENT
        assert subsession.session.num_participants == 6 * len(C.TREATMENT_CONFIG)

        communities = [enumerate([i] * 6) for i in range(len(C.TREATMENT_CONFIG))]
        communities = list(chain.from_iterable(communities))
        random.shuffle(communities)
        communities = iter(communities)

        treatments = [
            iter(
                random.sample(
                    ["high"] * (6 - community["L2"]) + ["low"] * (community["L2"]), k=6
                )
            )
            for community in C.TREATMENT_CONFIG
        ]

        ## AVATAR
        for player in subsession.get_players():
            id_in_community, community = next(communities)
            player.participant.vars["community"] = community
            player.participant.vars["id_in_community"] = id_in_community + 1
            player.participant.vars["treatment"] = next(treatments[community])
            player.participant.vars["remaining_learning"] = random.sample(
                range(C.NUM_SENTENCES), k=C.NUM_SENTENCES
            )
            player.participant.vars["num_correct"] = 0


def construct_stim_metadata(player, sentence: int):
    sentence_data = C.SENTENCE_DB.loc[sentence, :]
    metadata = {}

    ### SUBJECT
    if sentence_data.loc["pronoun"] == "anha":
        subject = C.AVATAR_PLAYER
    elif sentence_data.loc["pronoun"] == "yer":
        subject = C.AVATAR_PARTNER
    else:
        subject = C.AVATAR_OTHER

    if sentence_data.loc["verb"] == "dothra":
        metadata["rides"] = f"training/{subject}_rides.png"
        metadata["walks"] = ""
    else:
        metadata["rides"] = ""
        metadata["walks"] = f"training/{subject}_walks.png"

    ### OBJECT
    if sentence_data.loc["possessor"] == "an":
        possessor = C.AVATAR_PLAYER
    elif sentence_data.loc["possessor"] == "er":
        possessor = C.AVATAR_PARTNER
    else:
        possessor = C.AVATAR_OTHER

    if sentence_data.loc["possessum"] == "okr":
        metadata["tent"] = f"training/{possessor}_tent.png"
        metadata["brother"] = ""
    else:
        metadata["tent"] = ""
        metadata["brother"] = f"training/{possessor}_brother.png"

    ### CASE
    if sentence_data.loc["case"] == "aan":
        metadata["flip"] = "flip"
    else:
        metadata["flip"] = "dontflip"

    return metadata


PUNCTUATION = str.maketrans("", "", ".,!?;:")


def sentence_clean(sentence):
    return sentence.lower().translate(PUNCTUATION)


# PAGES
class Introduction(Page):
    @staticmethod
    def is_displayed(player: Player):
        return player.round_number == 1

    @staticmethod
    def vars_for_template(player: Player):
        return {
            "example_sentence": C.SENTENCE_DB.loc[
                12, f"{player.participant.vars['treatment']}_sentence"
            ],
            "treatment": player.participant.vars["treatment"],
            "training_trials": C.GOAL[player.participant.vars["treatment"]],
        }


class Learn(Page):
    form_model = "player"
    form_fields = ["answer"]

    @staticmethod
    def vars_for_template(player: Player):
        if player.field_maybe_none("sentence") is None:
            player.sentence = player.participant.vars["remaining_learning"].pop()

        return {
            "prompt": construct_stim_metadata(player, player.sentence),
            "num_correct": player.participant.vars["num_correct"],
            "treatment": player.participant.vars["treatment"],
            "percent_correct": int(
                player.participant.vars["num_correct"]
                / C.GOAL[player.participant.vars["treatment"]]
                * 100
            ),
            "goal": C.GOAL[player.participant.vars["treatment"]],
            "show_full_grammar": player.participant.vars["num_correct"] < 9,
        }

    def before_next_page(player, timeout_happened):
        player.correct = sentence_clean(player.answer) == sentence_clean(
            C.SENTENCE_DB.loc[
                player.sentence, f"{player.participant.vars['treatment']}_sentence"
            ]
        )
        if player.correct:
            player.participant.vars["num_correct"] += 1
        else:
            # wrong -> get this sentence again at the end
            player.participant.vars["remaining_learning"].insert(0, player.sentence)


class Feedback(Page):
    def vars_for_template(player):
        return {
            "prompt": construct_stim_metadata(player, player.sentence),
            "correct_sentence": C.SENTENCE_DB.loc[
                player.sentence, f"{player.participant.vars['treatment']}_sentence"
            ],
            "num_correct": player.participant.vars["num_correct"],
            "treatment": player.participant.vars["treatment"],
            "percent_correct": int(
                player.participant.vars["num_correct"]
                / C.GOAL[player.participant.vars["treatment"]]
                * 100
            ),
            "goal": C.GOAL[player.participant.vars["treatment"]],
            "show_full_grammar": player.participant.vars["num_correct"] < 9,
        }

    def app_after_this_page(player, upcoming_apps):
        if (
            player.participant.vars["num_correct"]
            >= C.GOAL[player.participant.vars["treatment"]]
        ):
            return upcoming_apps[0]


page_sequence = [Introduction, Learn, Feedback]

from otree.api import *
import random
import pandas as pd
import itertools
from typing import List, Sequence, Iterable


debug = False

doc = """
Communication phase of the dothraki experiment. Going till every participant has done 42 trials (21 sender, 21 receiver). 
"""


class C(BaseConstants):
    NAME_IN_URL = "use_dothraki"
    PLAYERS_PER_GROUP = 2

    # set to a high enough number that we wont run out during the 2 hour session
    NUM_ROUNDS = 200
    ROUNDS_EACH = 200
    CONSECUTIVE_ROUNDS = 8

    NUM_SENTENCES = 72

    SENDER_ROLE = "SENDER"
    RECEIVER_ROLE = "RECEIVER"

    SENTENCE_DB = pd.read_csv("../dothraki/_static/dothraki_stim/sentences.csv")

    AVATAR_OPTIONS = "CDHKRY"

    SENTENCE_TYPES = ["easy"] * 7 + ["hard1"] + ["hard2"] + ["hard3"]

    TREATMENT_CONFIG = [
        # {'L2': 1,
        #  'connected': 'full',
        #  'grammar_community': 'case'},
        # {'L2': 3,
        #  'connected': 'min',
        #  'grammar_community': 'case'},
        {"L2": 3, "connected": "lattice", "grammar_community": "poss"}
    ]


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    community = models.IntegerField()
    sentence_type = models.StringField()
    answer = models.StringField()
    sentence = models.IntegerField()
    foil1 = models.IntegerField()
    foil2 = models.IntegerField()
    foil3 = models.IntegerField()
    guess = models.IntegerField(choices=range(4))
    correct = models.BooleanField()
    active = models.BooleanField()


class Player(BasePlayer):
    avatar = models.StringField()
    avatar_partner = models.StringField()


# FUNCTIONS
def creating_session(subsession: Subsession):
    if subsession.round_number == 1:
        for community in C.TREATMENT_CONFIG:
            if community["connected"] == "full":
                community["pairs_iter"] = gen_pairs_fully_connected()
            elif community["connected"] == "min":
                community["pairs_iter"] = gen_pairs_min_connected()
            elif community["connected"] == "lattice":
                community["pairs_iter"] = gen_pairs_lattice_connected()

            community["known_grammar"] = choose_grammar_examples(community)
            community["sentence_type_order"] = choose_sentence_type_order(community)
            community["sentence_iter"] = gen_sentences(community)
            community["avatar_iter"] = iter(
                random.sample(C.AVATAR_OPTIONS, k=len(C.AVATAR_OPTIONS))
            )
        if debug:
            print("Round 1 init complete")

    if not (subsession.round_number - 1) % C.CONSECUTIVE_ROUNDS:
        # round 1 and every C.CONSECUTIVE_ROUNDS rounds thereafter, switch partners
        pairs = []
        active_players = []  # for keeping track in min connected communities

        for cid, community in enumerate(C.TREATMENT_CONFIG):
            nextpairs = next(community["pairs_iter"])
            for pair in nextpairs:
                pairs.append(find_player_ids_by_community_and_id(subsession, cid, pair))
            if len(nextpairs) == 2:
                # in min connected communities, set the active bit for some groups to be skipped
                for p in itertools.chain(pairs[-1], pairs[-2]):
                    active_players.append(p)
                active_pairs = set([i for sublist in nextpairs for i in sublist])
                inactive_pair = set(range(1, 6 + 1)) - active_pairs
                pairs.append(
                    find_player_ids_by_community_and_id(subsession, cid, inactive_pair)
                )
            else:
                for p in itertools.chain(pairs[-1], pairs[-2], pairs[-3]):
                    # in most cases, all these are actively playing
                    # .. except for the ones that already did their 40 rounds
                    if not (
                        community["connected"] != "min"
                        and subsession.round_number >= C.ROUNDS_EACH
                    ):
                        active_players.append(p)

        subsession.set_group_matrix(pairs)
        for p in subsession.get_players():
            if p.group.field_maybe_none("active") is not None:
                continue
            if p.id_in_subsession in active_players:
                p.group.active = True
            else:
                p.group.active = False

    else:
        # rounds 2-C.CONSECUTIVE_ROUNDS, same groups ..
        subsession.group_like_round(subsession.round_number - 1)
        for p in subsession.get_players():
            if p.group.field_maybe_none("active") is None:
                p.group.active = p.in_round(subsession.round_number - 1).group.active

            # .. but switch roles in even cases
            if not subsession.round_number % 2:
                if p._role == C.SENDER_ROLE:
                    p._role = C.RECEIVER_ROLE
                else:
                    p._role = C.SENDER_ROLE

    if debug:
        print(f"Groups assigned for round {subsession.round_number}")

    for cid, community in enumerate(C.TREATMENT_CONFIG):
        sentence = next(community["sentence_iter"])
        foils = gen_foils(community, sentence, subsession.round_number)
        # if debug: print(f"Round {subsession.round_number} Group {cid} Sentence {s}")
        group_ids = list(range(1, 6 + 1))
        community_players = find_players_by_community_and_id(subsession, cid, group_ids)
        for gid, p in zip(group_ids, community_players):
            if p.round_number == 1:
                p.avatar = next(community["avatar_iter"])
                p.participant.vars["avatar"] = p.avatar
            else:
                p.avatar = p.in_round(1).avatar
            if p._role == C.SENDER_ROLE:
                p.group.community = cid
                p.group.sentence = sentence
                p.group.sentence_type = community["sentence_type_order"][
                    subsession.round_number - 1
                ]
                p.group.foil1, p.group.foil2, p.group.foil3 = foils

        for p in community_players:
            p.avatar_partner = p.get_others_in_group()[0].avatar

    if debug:
        print("Generated sentences and foils")


def choose_sentence_type_order(community: dict) -> List[str]:
    gen_sentence_types = chain_random(C.SENTENCE_TYPES)
    return [next(gen_sentence_types) for _ in range(C.NUM_ROUNDS)]


def choose_grammar_examples(community: dict) -> dict:
    return {
        "case": random.choice(["aan", "oon"])
        if community["grammar_community"] != "case"
        else None,
        "pronoun": random.choice(["anha", "yer", "ma"])
        if community["grammar_community"] != "pronoun"
        else None,
        "poss": random.choice(["er", "an", "ma"])
        if community["grammar_community"] != "poss"
        else None,
    }


def find_players_by_community_and_id(
    subsession, cid: int, pair: List[int]
) -> List[Player]:
    return [
        p
        for p in subsession.get_players()
        if p.participant.vars["community"] == cid
        and p.participant.vars["id_in_community"] in pair
    ]


def find_player_ids_by_community_and_id(
    subsession, cid: int, pair: List[int]
) -> List[int]:
    return [
        p.id_in_subsession
        for p in find_players_by_community_and_id(subsession, cid, pair)
    ]


def gen_pairs_fully_connected() -> Iterable[List[List[int]]]:
    groupings = [
        [[1, 2], [3, 4], [5, 6]],
        [[1, 3], [2, 5], [4, 6]],
        [[1, 4], [2, 6], [3, 5]],
        [[1, 5], [2, 4], [3, 6]],
        [[1, 6], [2, 3], [4, 5]],
    ]
    return gen_pairs(groupings)


def gen_pairs_min_connected() -> Iterable[List[List[int]]]:
    groupings = [
        [[1, 2], [3, 4], [5, 6]],
        [[1, 3], [4, 6]],  # 2,5 not connected
        [[2, 3], [4, 5]],  # 1,6 not connected
        [[1, 2], [5, 6]],
    ]  # 3,4 on break for the others to catch up

    return gen_pairs(groupings)
    # raise KeyError("Only fully connected communitys are supported for now")


def gen_pairs_lattice_connected() -> Iterable[List[List[int]]]:
    groupings = [
        [[1, 2], [3, 4], [5, 6]],
        [[1, 3], [2, 6], [4, 5]],
        [[1, 5], [2, 3], [4, 6]],
        [[1, 6], [2, 4], [3, 5]],
    ]
    return gen_pairs(groupings)


def gen_pairs(groupings: List[List[List[int]]]) -> Iterable[List[List[int]]]:
    while True:
        for grouping in random.sample(groupings, k=len(groupings)):
            yield [random.sample(pair, k=2) for pair in grouping]


def chain_random(x: Sequence) -> Iterable:
    x = x[:]
    while True:
        for item in random.sample(x, k=len(x)):
            yield item


def gen_sentences(community: dict) -> Iterable[int]:
    if community["grammar_community"] == "case":
        selections = {
            "easy": chain_random(
                C.SENTENCE_DB.loc[
                    (C.SENTENCE_DB["pronoun"] == community["known_grammar"]["pronoun"])
                    & (
                        C.SENTENCE_DB["possessor"] == community["known_grammar"]["poss"]
                    ),
                    :,
                ].index.values.tolist()
            ),
            "hard1": chain_random(
                C.SENTENCE_DB.loc[
                    (C.SENTENCE_DB["pronoun"] == community["known_grammar"]["pronoun"])
                    & (
                        C.SENTENCE_DB["possessor"] != community["known_grammar"]["poss"]
                    ),
                    :,
                ].index.values.tolist()
            ),
            "hard2": chain_random(
                C.SENTENCE_DB.loc[
                    (C.SENTENCE_DB["pronoun"] != community["known_grammar"]["pronoun"])
                    & (
                        C.SENTENCE_DB["possessor"] == community["known_grammar"]["poss"]
                    ),
                    :,
                ].index.values.tolist()
            ),
            "hard3": chain_random(
                C.SENTENCE_DB.loc[
                    (C.SENTENCE_DB["pronoun"] != community["known_grammar"]["pronoun"])
                    & (
                        C.SENTENCE_DB["possessor"] != community["known_grammar"]["poss"]
                    ),
                    :,
                ].index.values.tolist()
            ),
        }
        # if debug: print("Generating sentences for community from selection:", selection)

    elif community["grammar_community"] == "poss":
        selections = {
            "easy": chain_random(
                C.SENTENCE_DB.loc[
                    (C.SENTENCE_DB["pronoun"] == community["known_grammar"]["pronoun"])
                    & (C.SENTENCE_DB["case"] == community["known_grammar"]["case"]),
                    :,
                ].index.values.tolist()
            ),
            "hard1": chain_random(
                C.SENTENCE_DB.loc[
                    (C.SENTENCE_DB["pronoun"] == community["known_grammar"]["pronoun"])
                    & (C.SENTENCE_DB["case"] != community["known_grammar"]["case"]),
                    :,
                ].index.values.tolist()
            ),
            "hard2": chain_random(
                C.SENTENCE_DB.loc[
                    (C.SENTENCE_DB["pronoun"] != community["known_grammar"]["pronoun"])
                    & (C.SENTENCE_DB["case"] == community["known_grammar"]["case"]),
                    :,
                ].index.values.tolist()
            ),
            "hard3": chain_random(
                C.SENTENCE_DB.loc[
                    (C.SENTENCE_DB["pronoun"] != community["known_grammar"]["pronoun"])
                    & (C.SENTENCE_DB["case"] != community["known_grammar"]["case"]),
                    :,
                ].index.values.tolist()
            ),
        }
    # return chain_random(selection_easy.index.values.tolist())
    else:
        # todo expand to other grammar communitys
        raise KeyError('only "case" community defined for now')

    for sentence_type in community["sentence_type_order"]:
        yield next(selections[sentence_type])


def gen_foils(community: dict, sentence: int, round_number: int) -> List[int]:
    sentence_type = community["sentence_type_order"][round_number - 1]
    if community["grammar_community"] == "case":
        if sentence_type == "easy":
            fixed_lex = random.choice(["verb", "possessum"])
            selection = C.SENTENCE_DB.loc[
                (C.SENTENCE_DB["pronoun"] == community["known_grammar"]["pronoun"])
                & (C.SENTENCE_DB["possessor"] == community["known_grammar"]["poss"])
                & (C.SENTENCE_DB[fixed_lex] == C.SENTENCE_DB.at[sentence, fixed_lex])
                & (~C.SENTENCE_DB.index.isin([sentence])),
                :,
            ]
        elif sentence_type == "hard1":
            selection = C.SENTENCE_DB.loc[
                (C.SENTENCE_DB["pronoun"] == community["known_grammar"]["pronoun"])
                & (C.SENTENCE_DB["verb"] == C.SENTENCE_DB.at[sentence, "verb"])
                & (
                    C.SENTENCE_DB["possessum"]
                    == C.SENTENCE_DB.at[sentence, "possessum"]
                )
                & (~C.SENTENCE_DB.index.isin([sentence])),
                :,
            ]
        elif sentence_type == "hard2":
            selection = C.SENTENCE_DB.loc[
                (C.SENTENCE_DB["possessor"] == community["known_grammar"]["poss"])
                & (C.SENTENCE_DB["verb"] == C.SENTENCE_DB.at[sentence, "verb"])
                & (
                    C.SENTENCE_DB["possessum"]
                    == C.SENTENCE_DB.at[sentence, "possessum"]
                )
                & (~C.SENTENCE_DB.index.isin([sentence])),
                :,
            ]
        elif sentence_type == "hard3":
            selection = C.SENTENCE_DB.loc[
                (C.SENTENCE_DB["case"] == C.SENTENCE_DB.at[sentence, "case"])
                & (C.SENTENCE_DB["verb"] == C.SENTENCE_DB.at[sentence, "verb"])
                & (
                    C.SENTENCE_DB["possessum"]
                    == C.SENTENCE_DB.at[sentence, "possessum"]
                )
                & (~C.SENTENCE_DB.index.isin([sentence])),
                :,
            ]

    elif community["grammar_community"] == "poss":
        if sentence_type == "easy":  # poss, and either verb or obj
            fixed_lex = random.choice(["verb", "possessum"])
            selection = C.SENTENCE_DB.loc[
                (C.SENTENCE_DB["pronoun"] == community["known_grammar"]["pronoun"])
                & (C.SENTENCE_DB["case"] == community["known_grammar"]["case"])
                & (C.SENTENCE_DB[fixed_lex] == C.SENTENCE_DB.at[sentence, fixed_lex])
                & (~C.SENTENCE_DB.index.isin([sentence])),
                :,
            ]
        elif sentence_type == "hard1":  # case, poss
            selection = C.SENTENCE_DB.loc[
                (C.SENTENCE_DB["pronoun"] == community["known_grammar"]["pronoun"])
                & (C.SENTENCE_DB["verb"] == C.SENTENCE_DB.at[sentence, "verb"])
                & (
                    C.SENTENCE_DB["possessum"]
                    == C.SENTENCE_DB.at[sentence, "possessum"]
                )
                & (~C.SENTENCE_DB.index.isin([sentence])),
                :,
            ]
        elif sentence_type == "hard2":  # pronoun, poss
            selection = C.SENTENCE_DB.loc[
                (C.SENTENCE_DB["case"] == community["known_grammar"]["case"])
                & (C.SENTENCE_DB["verb"] == C.SENTENCE_DB.at[sentence, "verb"])
                & (
                    C.SENTENCE_DB["possessum"]
                    == C.SENTENCE_DB.at[sentence, "possessum"]
                )
                & (~C.SENTENCE_DB.index.isin([sentence])),
                :,
            ]
        elif sentence_type == "hard3":  # case, pronoun
            selection = C.SENTENCE_DB.loc[
                (C.SENTENCE_DB["possessor"] == C.SENTENCE_DB.at[sentence, "possessor"])
                & (C.SENTENCE_DB["verb"] == C.SENTENCE_DB.at[sentence, "verb"])
                & (
                    C.SENTENCE_DB["possessum"]
                    == C.SENTENCE_DB.at[sentence, "possessum"]
                )
                & (~C.SENTENCE_DB.index.isin([sentence])),
                :,
            ]

    else:
        # todo expand to other grammar communitys
        raise KeyError('only "case" community defined for now')

    result = random.sample(selection.index.values.tolist(), k=3)
    assert len(result) == 3
    return result


def random_other(player):
    return random.sample(
        C.AVATAR_OPTIONS.replace(player.participant.vars["avatar"], "").replace(
            player.avatar_partner, ""
        ),
        k=1,
    )[0]


def construct_stim_metadata(player: Player, sentence: int) -> dict:
    sentence_data = C.SENTENCE_DB.loc[sentence, :]
    metadata = {}

    ### SUBJECT
    if sentence_data.loc["pronoun"] == "anha":
        subject = player.participant.vars["avatar"]
    elif sentence_data.loc["pronoun"] == "yer":
        subject = player.avatar_partner
    else:
        ## GET OTHER avatar
        subject = random_other(player)

    if sentence_data.loc["verb"] == "dothra":
        metadata["rides"] = f"rides/{subject}_rides.png"
        metadata["walks"] = ""
    else:
        metadata["rides"] = ""
        metadata["walks"] = f"walks/{subject}_walks.png"

    ### OBJECT
    if sentence_data.loc["possessor"] == "an":
        possessor = player.participant.vars["avatar"]
    elif sentence_data.loc["possessor"] == "er":
        possessor = player.avatar_partner
    else:
        possessor = random_other(player)

    if sentence_data.loc["possessum"] == "okr":
        metadata["tent"] = f"tent/{possessor}_tent.png"
        metadata["brother"] = ""
    else:
        metadata["tent"] = ""
        metadata["brother"] = f"brother/{possessor}_brother.png"

    ### CASE
    if sentence_data.loc["case"] == "aan":
        metadata["flip"] = "flip"
    else:
        metadata["flip"] = "dontflip"

    return metadata


# PAGES
class WaitTraining(WaitPage):
    wait_for_all_groups = True
    body_text = "Waiting for all players to complete training. \n<br />Feel free to stretch your legs -- You will be notified by the experimenter as soon as we are ready to proceed."

    @staticmethod
    def is_displayed(player):
        return player.round_number == 1


class Instructions(Page):
    @staticmethod
    def is_displayed(player: Player):
        return player.round_number == 1


class Send(Page):
    form_model = "group"
    form_fields = ["answer"]

    @staticmethod
    def is_displayed(player):
        return player.role == C.SENDER_ROLE and player.group.active

    @staticmethod
    def vars_for_template(player: Player):
        partner = player.group.get_player_by_role(C.RECEIVER_ROLE)

        if player.field_maybe_none("avatar_partner") is None:
            player.avatar_partner = partner.participant.avatar

        return {"prompt": construct_stim_metadata(player, player.group.sentence)}


class Receive(Page):
    form_model = "group"
    form_fields = ["guess"]

    @staticmethod
    def is_displayed(player):
        return player.role == C.RECEIVER_ROLE and player.group.active

    @staticmethod
    def vars_for_template(player: Player):
        partner = player.group.get_player_by_role(C.SENDER_ROLE)

        if player.field_maybe_none("avatar_partner") is None:
            player.avatar_partner = partner.participant.avatar

        options = list(
            enumerate(
                [
                    construct_stim_metadata(partner, player.group.sentence),
                    construct_stim_metadata(partner, player.group.foil1),
                    construct_stim_metadata(partner, player.group.foil2),
                    construct_stim_metadata(partner, player.group.foil3),
                ]
            )
        )
        random.shuffle(options)

        return {
            "sentence": player.group.answer,
            "options": [{"value": k, "prompt": v} for k, v in options],
        }

    @staticmethod
    def before_next_page(player: Player, timeout_happened: bool):
        player.group.correct = player.group.guess == 0


class FeedbackSend(Page):
    @staticmethod
    def is_displayed(player):
        return player.role == C.SENDER_ROLE and player.group.active


class FeedbackReceive(Page):
    @staticmethod
    def is_displayed(player):
        return player.role == C.RECEIVER_ROLE and player.group.active


class WaitSend(WaitPage):
    template_name = "use_dothraki/WaitSend.html"

    @staticmethod
    def is_displayed(player):
        return player.group.active

    @staticmethod
    def vars_for_template(player: Player):
        partner = player.group.get_player_by_role(C.SENDER_ROLE)

        if player.field_maybe_none("avatar_partner") is None:
            player.avatar_partner = partner.participant.avatar


class WaitReceive(WaitPage):
    template_name = "use_dothraki/WaitReceive.html"

    @staticmethod
    def is_displayed(player):
        return player.group.active


class Goodbye(Page):
    @staticmethod
    def is_displayed(player):
        return player.round_number == C.NUM_ROUNDS


page_sequence = [
    WaitTraining,
    Instructions,
    Send,
    WaitSend,
    Receive,
    WaitReceive,
    FeedbackSend,
    FeedbackReceive,
    Goodbye,
]
